package com.ftl.oak.utils;

import java.io.InputStream;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.jackrabbit.JcrConstants;
import org.apache.jackrabbit.oak.jcr.session.PropertyImpl;

/**
 * Utilities for oak jcr including retrieving {@link Node} values explicitly like inputStream or mimeType.
 * @author Petros Siatos
 *
 */
public class OakUtils {

	public static InputStream getInputStream(Node node) throws PathNotFoundException, RepositoryException {
		return getPropertyImpl(getResourceNode(node), JcrConstants.JCR_DATA).getStream();
	}
	
	public static String getMimeType(Node node) throws ValueFormatException, PathNotFoundException, RepositoryException {
		return getPropertyImpl(getResourceNode(node), JcrConstants.JCR_MIMETYPE).getString();
	}
	
//	public static String getName(Node node) throws PathNotFoundException, RepositoryException {
//		return getPropertyImpl(getResourceNode(node), JcrConstants.JCR_NAME).getString();
//	}
	
	public static Node getResourceNode(Node node) throws PathNotFoundException, RepositoryException {
		return node.getNode(JcrConstants.JCR_CONTENT);
	}
	
	public static PropertyImpl getPropertyImpl(Node node, String value) throws PathNotFoundException, RepositoryException {
		return (PropertyImpl) node.getProperty(value);
	}

	
}
