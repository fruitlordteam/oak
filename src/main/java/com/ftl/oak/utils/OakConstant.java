package com.ftl.oak.utils;

/**
 * Constants for Jackrabbit 3 Oak.
 * 
 * Check JcrConstants {@link org.apache.jackrabbit.JcrConstants} and NodeType {@link javax.jcr.nodetype.NodeType}
 * 
 * @author Petros Siatos
 *
 */
public class OakConstant {
 }
