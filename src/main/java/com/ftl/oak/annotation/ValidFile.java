package com.ftl.oak.annotation;

import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ftl.oak.model.validation.FileValidator;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.ElementType.FIELD;

/**
 * Custom Hibernate Validator Annotation to check if a {@link MultipartFile} is valid.
 * (size, content-type)
 * 
 * @author Petros Siatos
 *
 */
@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = FileValidator.class)
public @interface ValidFile {
	
	public static final String DEFAULT_MESSAGE = "{org.hibernate.validator.constraints.ValidFile.message}";

	String message() default DEFAULT_MESSAGE;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
