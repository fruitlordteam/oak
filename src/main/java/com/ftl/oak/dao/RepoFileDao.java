package com.ftl.oak.dao;

import com.ftl.core.dao.ICoreDao;
import com.ftl.oak.model.RepoFile;

public interface RepoFileDao extends ICoreDao<RepoFile> {

	public RepoFile findByIdentifier(String identifier);
}
