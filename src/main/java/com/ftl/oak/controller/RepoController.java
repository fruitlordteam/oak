//package com.ftl.oak.controller;
//
//import java.io.InputStream;
//import java.io.OutputStream;
//
//import javax.annotation.PreDestroy;
//import javax.jcr.ItemNotFoundException;
//import javax.jcr.Node;
//import javax.jcr.RepositoryException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.ModelAndView;
//
//import com.ftl.core.annotation.Get;
//import com.ftl.core.annotation.Post;
//import com.ftl.oak.model.RepoFile;
//import com.ftl.oak.service.IOakRepository;
//import com.ftl.oak.service.RepoFileService;
//import com.ftl.oak.utils.OakUtils;
//
///**
// * Demo Controller for jcr oak
// * 
// * http://jackrabbit.apache.org/oak/docs/dos_and_donts.html
// * Pattern: One session for one request/operation
// * 
// * @author Petros Siatos
// *
// */
//@RequestMapping(RepoController.ROUTE_ROOT)
////@Controller
//public class RepoController {
//
//	// Routes
//	public static final String ROUTE_ROOT 		= "/repo";
//	public static final String ROUTE_LOGIN		= "/login";
//	public static final String ROUTE_UPLOAD		= "/upload";
//	public static final String ROUTE_UPLOAD_SUCCESS		= "/uploadSuccess";
//	public static final String ROUTE_DOWNLOAD	= "/download";
//	
//	
//	/**
//	 * Size of a byte buffer to read/write file
//	 */
//    private static final int BUFFER_SIZE = 4096;
//	
//	@Autowired private IOakRepository oakRepo;
//	@Autowired private RepoFileService repoFileService;
//	
//    @Get(ROUTE_LOGIN)
//    public ModelAndView login(ModelAndView mnv) {
//    	
//    	oakRepo.login();
//    	
//    	mnv.addObject("repoResponse", "hopefuly logged in!");
//    	mnv.setViewName("repo/login");
//    	return mnv;
//    }
//    
//    @Get(ROUTE_UPLOAD)
//    public ModelAndView uploadForm(ModelAndView mnv) {
//    	
//    	mnv.setViewName("repo/upload");
//    	return mnv;
//    }
//    
//    @Post(ROUTE_UPLOAD)
//    public ModelAndView handleFormUpload(@RequestParam("name") String name, @RequestParam("file") MultipartFile file, ModelAndView mnv) {
//    	
//		if (!file.isEmpty()) {
////			Node resNode = oakRepo.addFile(file, "test", "UTF-8");
////
////			RepoFile doc = new RepoFile();
////			doc.setDescription(name);
////
////			try {
////				doc.setIdentifier(resNode.getIdentifier());
////				mnv.addObject("fileID", resNode.getIdentifier());
////			} catch (RepositoryException e) {
////				e.printStackTrace();
////			}
////			
////			repoFileService.save(doc);
//
//			mnv.setViewName("redirect:uploadSuccess");
//		}
//		return mnv;
//	}
//    
//    @Get(ROUTE_UPLOAD_SUCCESS)
//    public ModelAndView uploadSuccess(ModelAndView mnv) {
//    	mnv.setViewName("repo/uploadSuccess");
//    	return mnv;
//    }
//
//    
//    // http://www.codejava.net/frameworks/spring/spring-mvc-sample-application-for-downloading-files
//    @Get(ROUTE_DOWNLOAD)
//    public void download(HttpServletRequest request, HttpServletResponse response) throws Exception {
//    	
//    	Node node = null;
//    	try {
//    		node = oakRepo.getByDocID("36faf2fb-34d8-40df-8118-b2e48c88673a");
//    	} catch(ItemNotFoundException ex){
//    		node = oakRepo.getByPath("/test/Sun-icon.png");
//    	}
//    	
//        response.setContentType(OakUtils.getMimeType(node));
////        response.setContentLength((int) 33444);
// 
//        // set headers for the response
//        String headerKey = "Content-Disposition";
//        String headerValue = String.format("attachment; filename=\"%s\"", "cheesus");
//        response.setHeader(headerKey, headerValue);
//
//        InputStream inputStream = OakUtils.getInputStream(node);
//        
//        // get output stream of the response
//        OutputStream outStream = response.getOutputStream();
// 
//        byte[] buffer = new byte[BUFFER_SIZE];
//        int bytesRead = -1;
// 
//        // write bytes read from the input stream into the output stream
//        while ((bytesRead = inputStream.read(buffer)) != -1) {
//            outStream.write(buffer, 0, bytesRead);
//        }
// 
//        inputStream.close();
//        outStream.close();
//    	
//    }
//    
//    @PreDestroy 
//    public void destroy() {
//    	oakRepo.getSession().logout();
//    }
//}
