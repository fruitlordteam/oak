package com.ftl.oak.model;

import java.util.Optional;

import com.ftl.core.utils.CoreConstant;
import com.ftl.oak.service.IRepoNodeService;

/**
 * A model that has a repository representation needs to implement this.
 * (It's service needs to implement {@link IRepoNodeService}
 * @author Petros Siatos
 *
 */
public interface IRepoNode {

	public String getNodeFolderName();
	public String getNodeName();

	public default Optional<IRepoNode> getParentNode(){
		return Optional.empty();
	}

	public default String getNodeFullPath() {
		if (!getParentNode().isPresent()) {
			return String.join(CoreConstant.STR_SLASH, getNodeFolderName(), getNodeName());
		} else {
			return String.join(CoreConstant.STR_SLASH, getParentNode().get().getNodeFullPath(),  getNodeFolderName(), getNodeName());
		}
	}

}