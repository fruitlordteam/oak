package com.ftl.oak.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.web.multipart.MultipartFile;

import com.ftl.oak.utils.OakUtils;

/**
 * An implementation of {@link MultipartFile} for a file from oak repository.
 * @author Petros Siatos
 *
 */
public class OakMultipartFile implements MultipartFile {

	public OakMultipartFile(Node node, RepoFile repoFile) throws RepositoryException, IOException {
		name		= node.getName();
		originalName = repoFile.getDescription();
		contentType = OakUtils.getMimeType(node);
		inputStream = OakUtils.getInputStream(node);
		
		// available() does Not work always as expected.
//		size		= ((Number) inputStream.available()).longValue();
		
		size		= repoFile.getSize();
		empty 		= size.compareTo(0l) <= 0l; // is this right?
	}
	
	private String name;
	private String originalName;
	private String contentType;
	private InputStream inputStream;
	public Long size;
	public boolean empty;
		
	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getOriginalFilename() {
		return originalName;
	}

	@Override
	public long getSize() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public byte[] getBytes() throws IOException {
		throw new NotYetImplementedException();
	}
	
	@Override
	public void transferTo(File arg0) throws IOException, IllegalStateException {
		throw new NotYetImplementedException();
	}

}
