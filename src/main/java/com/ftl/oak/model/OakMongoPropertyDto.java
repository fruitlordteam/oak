package com.ftl.oak.model;

import com.mongodb.MongoClientURI;

/**
 * Model that contains Mongo information for Jackrabbit Oak Repository.
 * @author Petros Siatos
 *
 */
public class OakMongoPropertyDto {

	private MongoClientURI uri;
	
	public OakMongoPropertyDto(MongoClientURI uri) {
		this.uri = uri;
	}

	public MongoClientURI getUri() {
		return uri;
	}

}
