package com.ftl.oak.model;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import com.ftl.core.model.CoreModel;

/**
 * Association with a file located in a jcr repository.
 * @author Petros Siatos
 *
 */
@Entity
@Table(name= RepoFile.TABLE)
@SequenceGenerator(name = RepoFile.ID_SEQ, sequenceName = RepoFile.SEQ, allocationSize = 1)
public class RepoFile extends CoreModel {
	
	public static final String TABLE		= "repo_file";
	public static final String SEQ 			= "repo_file_seq";
	
	// Parameters
	public static final String IDENTIFIER 	= "identifier";
	
	/**
	 * Jackrabbit identifier
	 * (Not needed atm)
	 */
	private String identifier;
	
	/**
	 * File size.
	 */
	private Long size;
	
	@Transient
	private MultipartFile multipartFile;
	
	public RepoFile() {}
	
	public RepoFile(MultipartFile multipartFile) {
		super();
		this.multipartFile = multipartFile;
		this.description = multipartFile.getOriginalFilename();
		this.size = multipartFile.getSize();
	}
	
	public RepoFile(String identifier) {
		super();
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}
	
}
