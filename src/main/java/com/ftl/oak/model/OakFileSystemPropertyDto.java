package com.ftl.oak.model;

/**
 * Model that contains File System information for Jackrabbit Oak Repository.
 * @author Petros Siatos
 *
 */
public class OakFileSystemPropertyDto {

	private String name;
	private String path;
	private Integer maxFileSize;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getMaxFileSize() {
		return maxFileSize;
	}

	public void setMaxFileSize(Integer maxFileSize) {
		this.maxFileSize = maxFileSize;
	}
}
