package com.ftl.oak.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.web.multipart.MultipartFile;

import com.ftl.core.service.IUniqueModelService;
import com.ftl.oak.annotation.ValidFile;
/**
 * Validator to check if this model already exists in database.
 * <br/>
 * Your Service needs to implement {@link IUniqueModelService}
 * <br/>
 * <br/>
 * http://codingexplained.com/coding/java/hibernate/unique-field-validation-using-hibernate-spring
 * @author Petros Siatos
 *
 */
public class FileValidator implements ConstraintValidator<ValidFile, MultipartFile> {
	
	@Override
	public void initialize(ValidFile annotation) {
	}

	@Override
	public boolean isValid(MultipartFile file, ConstraintValidatorContext context) {
		if (file.isEmpty()) {
			return Boolean.FALSE;
		}  else {
			return Boolean.TRUE;
		}
	}
	
}