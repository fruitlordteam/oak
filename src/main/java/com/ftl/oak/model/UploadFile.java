package com.ftl.oak.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import com.ftl.oak.annotation.ValidFile;

/**
 * Simple model for uploading files includes validation.
 * 
 * @author Petros Siatos
 *
 */
public class UploadFile {

	@NotEmpty
	private String description;
	
	@ValidFile
	private MultipartFile file;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

}
