package com.ftl.oak.service;

import java.io.File;
import java.io.IOException;

import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

import org.apache.jackrabbit.oak.Oak;
import org.apache.jackrabbit.oak.jcr.Jcr;
import org.apache.jackrabbit.oak.plugins.segment.SegmentNodeStore;
import org.apache.jackrabbit.oak.plugins.segment.file.FileStore;

import com.ftl.oak.model.OakFileSystemPropertyDto;

/**
 * Extend this class to implement Jackrabbit Oak Repository using the file system.
 * @author Petros Siatos
 *
 */
abstract public class OakRepoImplFileSystem implements IOakRepository {
	
	private FileStore 	store;
	private Repository 	repo;
	private Session 	session;

	public OakRepoImplFileSystem(OakFileSystemPropertyDto oakDto) {

		try {
			store = getStore(oakDto);
 			Jcr jcr = new Jcr(new Oak(new SegmentNodeStore(store)));
			repo = jcr.createRepository();
			init();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			System.out.println("Mhpws ekanes malakia esy pou eftiakses thn init?");
		}

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				store.close();
			}
		}));
	}
	
	/**
	 * Any Folders that need to be initialized with the repo.
	 */
	abstract public void init() throws RepositoryException;
	
	private FileStore getStore(OakFileSystemPropertyDto oakDto) throws IOException {
		File directory = new File(oakDto.getPath(), oakDto.getName());
		return FileStore.newFileStore(directory)
				.withMaxFileSize(oakDto.getMaxFileSize())
				.withMemoryMapping(false)
				.create();
	}
	
	public void login() {
		try {
			session = repo.login(new SimpleCredentials("admin", "admin".toCharArray()));
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Session getSession() {
		return session;
	}
}
