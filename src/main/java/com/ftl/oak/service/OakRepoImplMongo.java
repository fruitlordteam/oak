package com.ftl.oak.service;

import java.io.IOException;

import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

import org.apache.jackrabbit.oak.Oak;
import org.apache.jackrabbit.oak.jcr.Jcr;
import org.apache.jackrabbit.oak.plugins.document.DocumentMK;
import org.apache.jackrabbit.oak.plugins.document.DocumentNodeStore;

import com.ftl.core.utils.CoreConstant;
import com.ftl.oak.model.OakMongoPropertyDto;
import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * Extend this class to implement Jackrabbit Oak Repository using MongoDB
 * @author Petros Siatos
 *
 */
abstract public class OakRepoImplMongo implements IOakRepository {
	
	private DocumentNodeStore 	store;
	private MongoClient client;
	private Repository 	repo;
	private Session 	session;

	public OakRepoImplMongo(OakMongoPropertyDto oakDto) {

		try {
			store = getStore(oakDto);
			repo = new Jcr(new Oak(store)).createRepository();
			init();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			System.out.println("Mhpws ekanes malakia esy pou eftiakses thn init?");
		}

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				store.dispose();
				client.close();
			}
		}));
	}
	
	/**
	 * Any Folders that need to be initialized with the repo.
	 */
	abstract public void init() throws RepositoryException;
	
	@SuppressWarnings(CoreConstant.WARNING_DEPRECATION)
	private DocumentNodeStore getStore(OakMongoPropertyDto oakDto) throws IOException {
		
		client = new MongoClient(oakDto.getUri()); 
		
		// TODO: to be removed when jcr oak uses MongoDatabase instead.
		DB db = client.getDB(oakDto.getUri().getDatabase());
		return new DocumentMK.Builder().setMongoDB(db).getNodeStore();
	}
	
	public void login() {
		try {
			session = repo.login(new SimpleCredentials("admin", "admin".toCharArray()));
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Session getSession() {
		return session;
	}
	
}
