package com.ftl.oak.service;

import java.io.IOException;

import javax.jcr.RepositoryException;

import com.ftl.oak.model.IRepoNode;

/**
 * A Service for a {@link IRepoNode} needs to implement this.
 *
 *	TODO: use SPRING AOP ?
 *
 * @author Petros Siatos
 *
 * @param <N> the Model implementing {@link IRepoNode}
 */
public interface IRepoNodeService<M extends IRepoNode> {

	public void add(M model) throws RepositoryException, IOException;
	public void remove(M model) throws RepositoryException;
	// TODO: java8 lambda expression?
	// http://stackoverflow.com/questions/13604703/how-do-i-define-a-method-which-takes-a-lambda-as-a-parameter-in-java-8
	public void addNode(M model) throws RepositoryException, IOException;
	public void removeNode(M model) throws RepositoryException;
		
}