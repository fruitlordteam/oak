package com.ftl.oak.service;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

/**
 * The interface to use for an oak repository
 * @author Petros Siatos
 *
 */
public interface IOakRepository {
	
	public void login();
	public Session getSession();
	
	public default void logout() {
		getSession().logout();
	}
	
	public default void saveAndLogout() throws AccessDeniedException, ItemExistsException, ReferentialIntegrityException, ConstraintViolationException, InvalidItemStateException, VersionException, LockException, NoSuchNodeTypeException, RepositoryException {
		getSession().save();
		getSession().logout();
	}

	public default Node getRoot() throws RepositoryException {
		return getSession().getRootNode();
	}
	
	public default Node getByDocID(String string) throws ItemNotFoundException, RepositoryException {
		return getSession().getNodeByIdentifier(string);
	}

	public default Node getByPath(String path) throws RepositoryException {
		return getRoot().getNode(path);
	}
	
}
