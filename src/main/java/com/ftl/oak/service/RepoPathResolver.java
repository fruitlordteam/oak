package com.ftl.oak.service;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.beans.factory.annotation.Autowired;

import com.ftl.oak.model.IRepoNode;

abstract public class RepoPathResolver<T extends IRepoNode> {

	@Autowired protected OakRepoImplFileSystem repo;
	
	abstract public Node getNode(T file) throws RepositoryException;
}