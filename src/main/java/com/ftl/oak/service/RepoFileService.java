package com.ftl.oak.service;

import org.springframework.stereotype.Service;

import com.ftl.core.service.CoreService;
import com.ftl.oak.dao.RepoFileDao;
import com.ftl.oak.model.RepoFile;

@Service
public class RepoFileService extends CoreService<RepoFile, RepoFileDao> {
	
	public RepoFile findByIdentifier(String identifier) {
		return getDao().findByIdentifier(identifier);
	}
}
