package com.ftl.oak.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(OakConfig.OAK_ROOT_PACKAGE)
@EnableJpaRepositories(OakConfig.OAK_DAO_PACKAGE)
class OakConfig {
	
	protected static final String OAK_ROOT_PACKAGE = "com.ftl.oak";
	protected static final String OAK_DAO_PACKAGE = "com.ftl.oak.dao";
	
}
